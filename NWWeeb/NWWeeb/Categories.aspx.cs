﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NWWeeb
{
    public partial class Categories : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NorthwindEntities ne = new NorthwindEntities();
            this.GridView1.DataSource = ne.Categories.ToList();
            this.GridView1.DataBind();
        }
    }
}