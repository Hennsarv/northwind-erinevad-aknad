﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NWWeeb
{
    public partial class Products : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var catID = Request.QueryString["CategoryID"];

            int intID = 0;
            if (int.TryParse(catID, out intID)) ;



            NorthwindEntities ne = new NorthwindEntities();
            this.griidikeTooted.DataSource = ne.Products
                .Where(x => (intID == 0) || (intID == x.CategoryID))
                .ToList();
            griidikeTooted.DataBind();

            
        }
    }
}