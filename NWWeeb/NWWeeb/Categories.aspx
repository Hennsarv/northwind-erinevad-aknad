﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Categories.aspx.cs" Inherits="NWWeeb.Categories" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" >
        <Columns>
            <asp:BoundField DataField="CategoryID" HeaderText="ID">
            </asp:BoundField>
            <asp:BoundField DataField="CategoryName" />


            <asp:HyperLinkField Text="Tooted"
                      DataNavigateUrlFormatString="~/Products.aspx?CategoryID={0}"
                      DataNavigateUrlFields="CategoryID" />         
        </Columns>
    </asp:GridView>
</asp:Content>
