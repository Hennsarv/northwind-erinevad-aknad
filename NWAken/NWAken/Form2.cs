﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NWAken
{
    public partial class Form2 : Form
    {
        internal List<int> Valitud;
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            List<Product> tooted;
            if (Valitud.Count == 0)
                tooted = Program.Ne.Products.ToList();
            else
                tooted = Program.Ne.Products
                    .Where(x => Valitud.Contains(x.CategoryID ?? 0))
                    .ToList();

            this.dataGridView1.DataSource = tooted;
        }
    }
}
