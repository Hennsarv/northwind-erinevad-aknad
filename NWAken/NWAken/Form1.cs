﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace NWAken
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
            if (Program.Ne == null) Program.Ne = new NorthwindEntities();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = Program.Ne.Categories.ToList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<int> valitud = new List<int>();
            var rows = dataGridView1.SelectedRows;
            foreach(DataGridViewRow x in rows)
            {
                valitud.Add(((Category)x.DataBoundItem).CategoryID);
            }

            Form2 f2 = new Form2();
            f2.Valitud = valitud;
            f2.Show(); // ShowDialog() paneks 'põhiakna' niikauaks seisma

        }
    }
}
