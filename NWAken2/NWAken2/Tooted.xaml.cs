﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NWAken2
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Tooted : Window
    {
        internal List<int> Kategooriad;

        public Tooted()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            List<Product> tooted;
            if (Kategooriad.Count == 0)
                tooted = MainWindow.ne.Products.ToList();
            else
                tooted = MainWindow.ne.Products
                    .Where(x => Kategooriad.Contains(x.CategoryID ?? 0))
                    .ToList();

            this.griidikeTooted.ItemsSource = tooted;
                    
        }
    }
}
