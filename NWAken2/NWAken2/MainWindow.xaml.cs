﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NWAken2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        internal static NorthwindEntities ne;

        public MainWindow()
        {
            InitializeComponent();
            if (ne == null)
            ne = new NorthwindEntities();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            this.griidike.ItemsSource = ne.Categories.ToList();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            List<int> kategooriad = new List<int>();
            foreach (Category c in griidike.SelectedItems)
            {
                kategooriad.Add(c.CategoryID);
            }

            Tooted t = new Tooted();
            t.Kategooriad = kategooriad;
            t.ShowDialog();
        }
    }
}
